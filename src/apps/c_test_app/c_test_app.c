#include <stdio.h>
#include "../../lib/cloudbase_client.h"

int main(void)
{
    cb_client_init();
    cb_client_install_signal_handler();

    mavlink_message_t msg;
    mavlink_msg_heartbeat_pack(1, 200, &msg, MAV_TYPE_HELICOPTER, MAV_AUTOPILOT_GENERIC, MAV_MODE_GUIDED_ARMED, 0, MAV_STATE_ACTIVE);

    cb_client_send_control_msg(msg);

    cb_client_loop();

    printf("\n%d\n", cb_client_app_id_from_name("autopilot_mavlink"));

    cb_client_teardown();
    return 0;
}
