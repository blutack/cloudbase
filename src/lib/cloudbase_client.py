import os, select, threading, time
import nanomsg as nm
import mavlink

class fifo(object):
    def __init__(self):
        self.buf = []
    def write(self, data):
        self.buf += data
        return len(data)
    def read(self):
        return self.buf.pop(0)

class CBReceive(threading.Thread):
    endpoint = None
    socket = None
    callback = None

    def __init__(self, endpoint, callback):
        super(Client, self).__init__()
        self.daemon = True

        self.endpoint = endpoint
        self.socket = nm.Socket(nm.SUB)
        self.socket.connect(self.endpoint)

        self.callback = callback

        self.start()

    def run(self):
        while True:
            self.callback(socket.recv())
            
class CBSend():
    endpoint = None
    socket = None

    def __init__(self, endpoint):
        self.endpoint = endpoint
        self.socket = nm.Socket(nm.PUB)
        self.socket.bind(self.endpoint)
        
    def send(data):
        self.socket.send(data)

class CBClient(threading.Thread):
    endpoint = 'ipc:///tmp/apps.ipc'

    socket = None
    mav = None
    f = None
    
    mavlink = mavlink
    
    apps = {}
    callbacks = {}
    self_callbacks = {} # Call back in the same process?

    def __init__(self, initial = False):
        self.endpoint = os.getenv('CB_ENDPOINT', self.endpoint)
        
        self.socket = nm.Socket(nm.BUS)
        if initial:
            self.socket.bind(self.endpoint)
        else:
            self.socket.connect(self.endpoint)
        print "Bound to " + self.endpoint
        
        self.f = fifo()
        self.mav = mavlink.MAVLink(self.f)
        
        super(CBClient, self).__init__()
        self.daemon = True
        self.start()
    
    def run(self):
        self.notify_msg(mavlink.MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE, self.__directory_services, self_callback = True)
        while True:
                msg = self.mav.decode(self.socket.recv())
                if(self.callbacks.get(msg.get_msgId())):
                    for cb in self.callbacks[msg.get_msgId()]:
                        cb(msg)
                else:
                    time.sleep(0.1)

    def loop(self):
        while threading.active_count() > 0:
            time.sleep(0.1)

    def send_msg(self, msg):
        self.socket.send(msg.pack(self.mav))

        if self.self_callbacks.get(msg.get_msgId()):
            for cb in self.self_callbacks.get(msg.get_msgId()):
                cb(msg)

    def notify_msg(self, msg_id, callback, self_callback = False):
        self.callbacks[msg_id] = [callback]
        if self_callback:
            self.self_callbacks[msg_id] = [callback]
        
    def __directory_services(self, msg):
        streams = []

        streams.append(msg.stream1)
        streams.append(msg.stream2)
        streams.append(msg.stream3)
        streams.append(msg.stream4)
        
        streams = filter(None, streams)
            
        self.apps[msg.app_id] = {'app_name': msg.app_name, 'streams': streams}

    def request_stream(self, app_id, stream, callback):
        endpoint = self.__request_stream_addr(app_id, stream)
        
        return CBReceive(endpoint, callback)

    def __request_stream_addr(app_id, stream):
        return self.apps[app_id]['streams'][stream]
        
    def start_stream(self, stream_idx):
        if os.environ.get('STREAM' + str(stream_idx)):
            return CBSend(os.environ.get('STREAM' + str(stream_idx_)))
