#ifndef CLOUDBASE_H_INCLUDED
#define CLOUDBASE_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <pthread.h>
#include <signal.h>

#include "mavlink/cloudbase/mavlink.h"

#include <nanomsg/nn.h>
#include <nanomsg/bus.h>

#define CB_CLIENT_BUFFER_LENGTH 1024
#define CB_CLIENT_MAX_CALLBACKS 32
#define CB_CLIENT_MAX_STREAMS 4
#define CB_CLIENT_STREAM_ADDRESS_LENGTH 32
#define CB_CLIENT_APP_NAME_LENGTH 32 + 1
#define CB_CLIENT_SOCKET_TIMEOUT 100
#define CB_CLIENT_

typedef struct
{
    char streams[CB_CLIENT_MAX_STREAMS][CB_CLIENT_STREAM_ADDRESS_LENGTH];
    char name[CB_CLIENT_APP_NAME_LENGTH];
} cb_client_app;

bool cb_client_running = true; // Global flag to trigger pthread server shutdowns

int32_t cb_client_sock; // TODO: rename this to control_sock
uint8_t cb_client_input_buffer[CB_CLIENT_BUFFER_LENGTH]; // Input buffer for mavlink

cb_client_app* cb_client_apps[255]; // Array of apps registered with the client

pthread_t cb_client_control_loop_thread; // Control loop thread; listens for app announcements & callbacks

void (*cb_client_control_callbacks[255]) (mavlink_message_t); // Yep, I suck at C datastructures

// List of received stream message callbacks
void (*cb_client_stream_callbacks[CB_CLIENT_MAX_STREAMS]) (uint8_t*);

// Function prototypes
void cb_client_init ();
void cb_client_install_signal_handler ();
void cb_client_signal_callback();
void cb_client_register_control_callback (uint8_t cb_client_msg_id, void (*f)(mavlink_message_t));
void *cb_client_control_loop ();
void cb_client_directory_services (mavlink_message_t);
void cb_client_signal_callback ();

void cb_client_init ()
{
    cb_client_sock = nn_socket (AF_SP, NN_BUS);
    assert (getenv ("CB_ENDPOINT") != 0);
    
    int err = nn_connect (cb_client_sock, getenv ("CB_ENDPOINT"));
    assert(err >= 0);
    
    int32_t cb_client_timeout = CB_CLIENT_SOCKET_TIMEOUT;
    nn_setsockopt (cb_client_sock, NN_SOL_SOCKET, NN_RCVTIMEO, &cb_client_timeout, sizeof(int));
    
    err = pthread_create(&cb_client_control_loop_thread, NULL, &cb_client_control_loop, NULL);
    assert(err == 0);
    
    cb_client_register_control_callback(MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE, cb_client_directory_services);
}

void cb_client_install_signal_handler ()
{
    signal(SIGINT, cb_client_signal_callback);
}

void cb_client_signal_callback()
{
    cb_client_running = false;
}

void cb_client_send_control_msg (mavlink_message_t cb_client_msg)
{
    int32_t len = mavlink_msg_to_send_buffer (cb_client_input_buffer, &cb_client_msg);
    assert (nn_send (cb_client_sock, &cb_client_input_buffer, len, 0) == len);
}

void cb_client_register_control_callback (uint8_t cb_client_msg_id, void (*f)(mavlink_message_t))
{
    cb_client_control_callbacks[cb_client_msg_id] = f;
}

void cb_client_directory_services(mavlink_message_t msg)
{
    int app_id = mavlink_msg_cb_app_list_response_get_app_id(&msg);

    if(cb_client_apps[app_id] != 0)
    {
        free(cb_client_apps[app_id]);
        cb_client_apps[app_id] = 0;
    }

    cb_client_app *app = malloc(sizeof(cb_client_app));

    mavlink_msg_cb_app_list_response_get_stream1(&msg, app->streams[0]);
    mavlink_msg_cb_app_list_response_get_stream2(&msg, app->streams[1]);
    mavlink_msg_cb_app_list_response_get_stream3(&msg, app->streams[2]);
    mavlink_msg_cb_app_list_response_get_stream4(&msg, app->streams[3]);

    mavlink_msg_cb_app_list_response_get_app_name(&msg, app->name);

    cb_client_apps[app_id] = app;
}

int cb_client_app_id_from_name(char* name)
{
    int i;

    for (i = 0; i < 255; i++)
    {
        if (cb_client_apps[i] != 0)
        {
            if (strcmp(cb_client_apps[i]->name, name) == 0)
            {
                return i;
            }
        }
    }

    return -1;
}

void cb_client_directory_services_teardown()
{
    uint i;
    for(i = 0; i < 255; i++)
    {
        if(cb_client_apps[i] != 0)
        {
            free(cb_client_apps[i]);
        }
    }
}

void cb_client_loop()
{
    pthread_join(cb_client_control_loop_thread, NULL);
}

void *cb_client_control_loop ()
{
    int recsize, i;
    mavlink_message_t msg;
    mavlink_status_t status;
    uint8_t *cb_client_output_buffer = NULL;

    while (cb_client_running)
    {
        recsize = nn_recv (cb_client_sock, &cb_client_output_buffer, NN_MSG, 0);

        if (recsize < 0)
        {
            assert (nn_errno() == EAGAIN);
        }
        else
        {
            for (i = 0; i < recsize; i++)
            {
                if (mavlink_parse_char(MAVLINK_COMM_0, cb_client_output_buffer[i], &msg, &status))
                {
                    printf("Received packet: SYS: %d, COMP: %d, LEN: %d, MSG ID: %d\n", msg.sysid, msg.compid, msg.len, msg.msgid);
                    if(cb_client_control_callbacks[msg.msgid] != 0)
                    {
                        (*cb_client_control_callbacks[msg.msgid])(msg);
                    }
                    fflush(stdout);
                }
            }
            nn_freemsg(cb_client_output_buffer);
        }
    }

    return NULL;
}

void cb_client_register_stream_callback(int app_id, int stream, void (*f)(uint8_t))
{
	// Work out address
	// allocate socket & connect
	// pass socket to thread
	assert(cb_client_apps[app_id] != 0);
	char* stream_addr = cb_client_apps[app_id]->streams[stream - 1]; // We 1 index the streams, but store zero indexed
	int32_t sock = nn_socket (AF_SP, NN_BUS);
	
	int err = nn_connect (sock, stream_addr);
    assert (err >= 0);
}

void *cb_client_data_loop (int stream_id)
{
	// Get socket from stream_id
	// Get callback from stream_id
    int recsize;
    uint8_t *cb_client_output_buffer = NULL;

    while (cb_client_running)
    {
        recsize = nn_recv (cb_client_sock, &cb_client_output_buffer, NN_MSG, 0);
        if (recsize < 0)
        {
            assert (nn_errno() == EAGAIN);
        }
        else
        {
            printf("%d", recsize);
            nn_freemsg(cb_client_output_buffer);
        }
    }
    
    nn_close (cb_stream_sock);

    return NULL;
}

void cb_client_teardown ()
{
    cb_client_running = false;
    nn_close (cb_client_sock);
    cb_client_directory_services_teardown();
    pthread_join(cb_client_control_loop_thread, NULL);
}

#endif
