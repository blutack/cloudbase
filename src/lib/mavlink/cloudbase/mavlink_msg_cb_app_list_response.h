// MESSAGE CB_APP_LIST_RESPONSE PACKING

#define MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE 152

typedef struct __mavlink_cb_app_list_response_t
{
 uint8_t app_id; ///< Unique ID of the application; not static.
 char app_name[32]; ///< Application name; not unique if multiple instances running.
 uint8_t app_type; ///< Application class.
 uint8_t streams_used; ///< Bitfield representing opened datastreams
 char stream1[32]; ///< Datastream one, if exists
 char stream2[32]; ///< Datastream two, if exists.
 char stream3[32]; ///< Datastream three, if exists.
 char stream4[32]; ///< Datastream four, if exists.
 uint8_t mavlink_version; ///< MAVLink version
} mavlink_cb_app_list_response_t;

#define MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_LEN 164
#define MAVLINK_MSG_ID_152_LEN 164

#define MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_CRC 41
#define MAVLINK_MSG_ID_152_CRC 41

#define MAVLINK_MSG_CB_APP_LIST_RESPONSE_FIELD_APP_NAME_LEN 32
#define MAVLINK_MSG_CB_APP_LIST_RESPONSE_FIELD_STREAM1_LEN 32
#define MAVLINK_MSG_CB_APP_LIST_RESPONSE_FIELD_STREAM2_LEN 32
#define MAVLINK_MSG_CB_APP_LIST_RESPONSE_FIELD_STREAM3_LEN 32
#define MAVLINK_MSG_CB_APP_LIST_RESPONSE_FIELD_STREAM4_LEN 32

#define MAVLINK_MESSAGE_INFO_CB_APP_LIST_RESPONSE { \
	"CB_APP_LIST_RESPONSE", \
	9, \
	{  { "app_id", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_cb_app_list_response_t, app_id) }, \
         { "app_name", NULL, MAVLINK_TYPE_CHAR, 32, 1, offsetof(mavlink_cb_app_list_response_t, app_name) }, \
         { "app_type", NULL, MAVLINK_TYPE_UINT8_T, 0, 33, offsetof(mavlink_cb_app_list_response_t, app_type) }, \
         { "streams_used", NULL, MAVLINK_TYPE_UINT8_T, 0, 34, offsetof(mavlink_cb_app_list_response_t, streams_used) }, \
         { "stream1", NULL, MAVLINK_TYPE_CHAR, 32, 35, offsetof(mavlink_cb_app_list_response_t, stream1) }, \
         { "stream2", NULL, MAVLINK_TYPE_CHAR, 32, 67, offsetof(mavlink_cb_app_list_response_t, stream2) }, \
         { "stream3", NULL, MAVLINK_TYPE_CHAR, 32, 99, offsetof(mavlink_cb_app_list_response_t, stream3) }, \
         { "stream4", NULL, MAVLINK_TYPE_CHAR, 32, 131, offsetof(mavlink_cb_app_list_response_t, stream4) }, \
         { "mavlink_version", NULL, MAVLINK_TYPE_UINT8_T, 0, 163, offsetof(mavlink_cb_app_list_response_t, mavlink_version) }, \
         } \
}


/**
 * @brief Pack a cb_app_list_response message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param app_id Unique ID of the application; not static.
 * @param app_name Application name; not unique if multiple instances running.
 * @param app_type Application class.
 * @param streams_used Bitfield representing opened datastreams
 * @param stream1 Datastream one, if exists
 * @param stream2 Datastream two, if exists.
 * @param stream3 Datastream three, if exists.
 * @param stream4 Datastream four, if exists.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_cb_app_list_response_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
						       uint8_t app_id, const char *app_name, uint8_t app_type, uint8_t streams_used, const char *stream1, const char *stream2, const char *stream3, const char *stream4)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_LEN];
	_mav_put_uint8_t(buf, 0, app_id);
	_mav_put_uint8_t(buf, 33, app_type);
	_mav_put_uint8_t(buf, 34, streams_used);
	_mav_put_uint8_t(buf, 163, 2);
	_mav_put_char_array(buf, 1, app_name, 32);
	_mav_put_char_array(buf, 35, stream1, 32);
	_mav_put_char_array(buf, 67, stream2, 32);
	_mav_put_char_array(buf, 99, stream3, 32);
	_mav_put_char_array(buf, 131, stream4, 32);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_LEN);
#else
	mavlink_cb_app_list_response_t packet;
	packet.app_id = app_id;
	packet.app_type = app_type;
	packet.streams_used = streams_used;
	packet.mavlink_version = 2;
	mav_array_memcpy(packet.app_name, app_name, sizeof(char)*32);
	mav_array_memcpy(packet.stream1, stream1, sizeof(char)*32);
	mav_array_memcpy(packet.stream2, stream2, sizeof(char)*32);
	mav_array_memcpy(packet.stream3, stream3, sizeof(char)*32);
	mav_array_memcpy(packet.stream4, stream4, sizeof(char)*32);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_LEN, MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_CRC);
#else
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_LEN);
#endif
}

/**
 * @brief Pack a cb_app_list_response message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param app_id Unique ID of the application; not static.
 * @param app_name Application name; not unique if multiple instances running.
 * @param app_type Application class.
 * @param streams_used Bitfield representing opened datastreams
 * @param stream1 Datastream one, if exists
 * @param stream2 Datastream two, if exists.
 * @param stream3 Datastream three, if exists.
 * @param stream4 Datastream four, if exists.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_cb_app_list_response_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
							   mavlink_message_t* msg,
						           uint8_t app_id,const char *app_name,uint8_t app_type,uint8_t streams_used,const char *stream1,const char *stream2,const char *stream3,const char *stream4)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_LEN];
	_mav_put_uint8_t(buf, 0, app_id);
	_mav_put_uint8_t(buf, 33, app_type);
	_mav_put_uint8_t(buf, 34, streams_used);
	_mav_put_uint8_t(buf, 163, 2);
	_mav_put_char_array(buf, 1, app_name, 32);
	_mav_put_char_array(buf, 35, stream1, 32);
	_mav_put_char_array(buf, 67, stream2, 32);
	_mav_put_char_array(buf, 99, stream3, 32);
	_mav_put_char_array(buf, 131, stream4, 32);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_LEN);
#else
	mavlink_cb_app_list_response_t packet;
	packet.app_id = app_id;
	packet.app_type = app_type;
	packet.streams_used = streams_used;
	packet.mavlink_version = 2;
	mav_array_memcpy(packet.app_name, app_name, sizeof(char)*32);
	mav_array_memcpy(packet.stream1, stream1, sizeof(char)*32);
	mav_array_memcpy(packet.stream2, stream2, sizeof(char)*32);
	mav_array_memcpy(packet.stream3, stream3, sizeof(char)*32);
	mav_array_memcpy(packet.stream4, stream4, sizeof(char)*32);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_LEN, MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_CRC);
#else
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_LEN);
#endif
}

/**
 * @brief Encode a cb_app_list_response struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param cb_app_list_response C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_cb_app_list_response_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_cb_app_list_response_t* cb_app_list_response)
{
	return mavlink_msg_cb_app_list_response_pack(system_id, component_id, msg, cb_app_list_response->app_id, cb_app_list_response->app_name, cb_app_list_response->app_type, cb_app_list_response->streams_used, cb_app_list_response->stream1, cb_app_list_response->stream2, cb_app_list_response->stream3, cb_app_list_response->stream4);
}

/**
 * @brief Encode a cb_app_list_response struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param cb_app_list_response C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_cb_app_list_response_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_cb_app_list_response_t* cb_app_list_response)
{
	return mavlink_msg_cb_app_list_response_pack_chan(system_id, component_id, chan, msg, cb_app_list_response->app_id, cb_app_list_response->app_name, cb_app_list_response->app_type, cb_app_list_response->streams_used, cb_app_list_response->stream1, cb_app_list_response->stream2, cb_app_list_response->stream3, cb_app_list_response->stream4);
}

/**
 * @brief Send a cb_app_list_response message
 * @param chan MAVLink channel to send the message
 *
 * @param app_id Unique ID of the application; not static.
 * @param app_name Application name; not unique if multiple instances running.
 * @param app_type Application class.
 * @param streams_used Bitfield representing opened datastreams
 * @param stream1 Datastream one, if exists
 * @param stream2 Datastream two, if exists.
 * @param stream3 Datastream three, if exists.
 * @param stream4 Datastream four, if exists.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_cb_app_list_response_send(mavlink_channel_t chan, uint8_t app_id, const char *app_name, uint8_t app_type, uint8_t streams_used, const char *stream1, const char *stream2, const char *stream3, const char *stream4)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_LEN];
	_mav_put_uint8_t(buf, 0, app_id);
	_mav_put_uint8_t(buf, 33, app_type);
	_mav_put_uint8_t(buf, 34, streams_used);
	_mav_put_uint8_t(buf, 163, 2);
	_mav_put_char_array(buf, 1, app_name, 32);
	_mav_put_char_array(buf, 35, stream1, 32);
	_mav_put_char_array(buf, 67, stream2, 32);
	_mav_put_char_array(buf, 99, stream3, 32);
	_mav_put_char_array(buf, 131, stream4, 32);
#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE, buf, MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_LEN, MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE, buf, MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_LEN);
#endif
#else
	mavlink_cb_app_list_response_t packet;
	packet.app_id = app_id;
	packet.app_type = app_type;
	packet.streams_used = streams_used;
	packet.mavlink_version = 2;
	mav_array_memcpy(packet.app_name, app_name, sizeof(char)*32);
	mav_array_memcpy(packet.stream1, stream1, sizeof(char)*32);
	mav_array_memcpy(packet.stream2, stream2, sizeof(char)*32);
	mav_array_memcpy(packet.stream3, stream3, sizeof(char)*32);
	mav_array_memcpy(packet.stream4, stream4, sizeof(char)*32);
#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE, (const char *)&packet, MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_LEN, MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE, (const char *)&packet, MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_LEN);
#endif
#endif
}

#endif

// MESSAGE CB_APP_LIST_RESPONSE UNPACKING


/**
 * @brief Get field app_id from cb_app_list_response message
 *
 * @return Unique ID of the application; not static.
 */
static inline uint8_t mavlink_msg_cb_app_list_response_get_app_id(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint8_t(msg,  0);
}

/**
 * @brief Get field app_name from cb_app_list_response message
 *
 * @return Application name; not unique if multiple instances running.
 */
static inline uint16_t mavlink_msg_cb_app_list_response_get_app_name(const mavlink_message_t* msg, char *app_name)
{
	return _MAV_RETURN_char_array(msg, app_name, 32,  1);
}

/**
 * @brief Get field app_type from cb_app_list_response message
 *
 * @return Application class.
 */
static inline uint8_t mavlink_msg_cb_app_list_response_get_app_type(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint8_t(msg,  33);
}

/**
 * @brief Get field streams_used from cb_app_list_response message
 *
 * @return Bitfield representing opened datastreams
 */
static inline uint8_t mavlink_msg_cb_app_list_response_get_streams_used(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint8_t(msg,  34);
}

/**
 * @brief Get field stream1 from cb_app_list_response message
 *
 * @return Datastream one, if exists
 */
static inline uint16_t mavlink_msg_cb_app_list_response_get_stream1(const mavlink_message_t* msg, char *stream1)
{
	return _MAV_RETURN_char_array(msg, stream1, 32,  35);
}

/**
 * @brief Get field stream2 from cb_app_list_response message
 *
 * @return Datastream two, if exists.
 */
static inline uint16_t mavlink_msg_cb_app_list_response_get_stream2(const mavlink_message_t* msg, char *stream2)
{
	return _MAV_RETURN_char_array(msg, stream2, 32,  67);
}

/**
 * @brief Get field stream3 from cb_app_list_response message
 *
 * @return Datastream three, if exists.
 */
static inline uint16_t mavlink_msg_cb_app_list_response_get_stream3(const mavlink_message_t* msg, char *stream3)
{
	return _MAV_RETURN_char_array(msg, stream3, 32,  99);
}

/**
 * @brief Get field stream4 from cb_app_list_response message
 *
 * @return Datastream four, if exists.
 */
static inline uint16_t mavlink_msg_cb_app_list_response_get_stream4(const mavlink_message_t* msg, char *stream4)
{
	return _MAV_RETURN_char_array(msg, stream4, 32,  131);
}

/**
 * @brief Get field mavlink_version from cb_app_list_response message
 *
 * @return MAVLink version
 */
static inline uint8_t mavlink_msg_cb_app_list_response_get_mavlink_version(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint8_t(msg,  163);
}

/**
 * @brief Decode a cb_app_list_response message into a struct
 *
 * @param msg The message to decode
 * @param cb_app_list_response C-struct to decode the message contents into
 */
static inline void mavlink_msg_cb_app_list_response_decode(const mavlink_message_t* msg, mavlink_cb_app_list_response_t* cb_app_list_response)
{
#if MAVLINK_NEED_BYTE_SWAP
	cb_app_list_response->app_id = mavlink_msg_cb_app_list_response_get_app_id(msg);
	mavlink_msg_cb_app_list_response_get_app_name(msg, cb_app_list_response->app_name);
	cb_app_list_response->app_type = mavlink_msg_cb_app_list_response_get_app_type(msg);
	cb_app_list_response->streams_used = mavlink_msg_cb_app_list_response_get_streams_used(msg);
	mavlink_msg_cb_app_list_response_get_stream1(msg, cb_app_list_response->stream1);
	mavlink_msg_cb_app_list_response_get_stream2(msg, cb_app_list_response->stream2);
	mavlink_msg_cb_app_list_response_get_stream3(msg, cb_app_list_response->stream3);
	mavlink_msg_cb_app_list_response_get_stream4(msg, cb_app_list_response->stream4);
	cb_app_list_response->mavlink_version = mavlink_msg_cb_app_list_response_get_mavlink_version(msg);
#else
	memcpy(cb_app_list_response, _MAV_PAYLOAD(msg), MAVLINK_MSG_ID_CB_APP_LIST_RESPONSE_LEN);
#endif
}
