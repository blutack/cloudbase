/** @file
 *	@brief MAVLink comm protocol testsuite generated from cloudbase.xml
 *	@see http://qgroundcontrol.org/mavlink/
 */
#ifndef CLOUDBASE_TESTSUITE_H
#define CLOUDBASE_TESTSUITE_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef MAVLINK_TEST_ALL
#define MAVLINK_TEST_ALL
static void mavlink_test_common(uint8_t, uint8_t, mavlink_message_t *last_msg);
static void mavlink_test_cloudbase(uint8_t, uint8_t, mavlink_message_t *last_msg);

static void mavlink_test_all(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_test_common(system_id, component_id, last_msg);
	mavlink_test_cloudbase(system_id, component_id, last_msg);
}
#endif

#include "../common/testsuite.h"


static void mavlink_test_cb_app_control(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_cb_app_control_t packet_in = {
		5,
	}72,
	}2,
	};
	mavlink_cb_app_control_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.app_id = packet_in.app_id;
        	packet1.action = packet_in.action;
        	packet1.mavlink_version = packet_in.mavlink_version;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_cb_app_control_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_cb_app_control_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_cb_app_control_pack(system_id, component_id, &msg , packet1.app_id , packet1.action );
	mavlink_msg_cb_app_control_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_cb_app_control_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.app_id , packet1.action );
	mavlink_msg_cb_app_control_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_cb_app_control_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_cb_app_control_send(MAVLINK_COMM_1 , packet1.app_id , packet1.action );
	mavlink_msg_cb_app_control_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_cb_app_list_request(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_cb_app_list_request_t packet_in = {
		5,
	}2,
	};
	mavlink_cb_app_list_request_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.app_id = packet_in.app_id;
        	packet1.mavlink_version = packet_in.mavlink_version;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_cb_app_list_request_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_cb_app_list_request_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_cb_app_list_request_pack(system_id, component_id, &msg , packet1.app_id );
	mavlink_msg_cb_app_list_request_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_cb_app_list_request_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.app_id );
	mavlink_msg_cb_app_list_request_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_cb_app_list_request_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_cb_app_list_request_send(MAVLINK_COMM_1 , packet1.app_id );
	mavlink_msg_cb_app_list_request_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_cb_app_list_response(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_cb_app_list_response_t packet_in = {
		5,
	}"BCDEFGHIJKLMNOPQRSTUVWXYZABCDEF",
	}168,
	}235,
	}"JKLMNOPQRSTUVWXYZABCDEFGHIJKLMN",
	}"PQRSTUVWXYZABCDEFGHIJKLMNOPQRST",
	}"VWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ",
	}"BCDEFGHIJKLMNOPQRSTUVWXYZABCDEF",
	}2,
	};
	mavlink_cb_app_list_response_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.app_id = packet_in.app_id;
        	packet1.app_type = packet_in.app_type;
        	packet1.streams_used = packet_in.streams_used;
        	packet1.mavlink_version = packet_in.mavlink_version;
        
        	mav_array_memcpy(packet1.app_name, packet_in.app_name, sizeof(char)*32);
        	mav_array_memcpy(packet1.stream1, packet_in.stream1, sizeof(char)*32);
        	mav_array_memcpy(packet1.stream2, packet_in.stream2, sizeof(char)*32);
        	mav_array_memcpy(packet1.stream3, packet_in.stream3, sizeof(char)*32);
        	mav_array_memcpy(packet1.stream4, packet_in.stream4, sizeof(char)*32);
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_cb_app_list_response_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_cb_app_list_response_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_cb_app_list_response_pack(system_id, component_id, &msg , packet1.app_id , packet1.app_name , packet1.app_type , packet1.streams_used , packet1.stream1 , packet1.stream2 , packet1.stream3 , packet1.stream4 );
	mavlink_msg_cb_app_list_response_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_cb_app_list_response_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.app_id , packet1.app_name , packet1.app_type , packet1.streams_used , packet1.stream1 , packet1.stream2 , packet1.stream3 , packet1.stream4 );
	mavlink_msg_cb_app_list_response_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_cb_app_list_response_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_cb_app_list_response_send(MAVLINK_COMM_1 , packet1.app_id , packet1.app_name , packet1.app_type , packet1.streams_used , packet1.stream1 , packet1.stream2 , packet1.stream3 , packet1.stream4 );
	mavlink_msg_cb_app_list_response_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_cloudbase(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_test_cb_app_control(system_id, component_id, last_msg);
	mavlink_test_cb_app_list_request(system_id, component_id, last_msg);
	mavlink_test_cb_app_list_response(system_id, component_id, last_msg);
}

#ifdef __cplusplus
}
#endif // __cplusplus
#endif // CLOUDBASE_TESTSUITE_H
