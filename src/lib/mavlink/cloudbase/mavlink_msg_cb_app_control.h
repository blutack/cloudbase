// MESSAGE CB_APP_CONTROL PACKING

#define MAVLINK_MSG_ID_CB_APP_CONTROL 150

typedef struct __mavlink_cb_app_control_t
{
 uint8_t app_id; ///< ID of the application; not static.
 uint8_t action; ///< Action to carry out. Defined in CB_APP_CONTROL_ACTION.
 uint8_t mavlink_version; ///< MAVLink version
} mavlink_cb_app_control_t;

#define MAVLINK_MSG_ID_CB_APP_CONTROL_LEN 3
#define MAVLINK_MSG_ID_150_LEN 3

#define MAVLINK_MSG_ID_CB_APP_CONTROL_CRC 254
#define MAVLINK_MSG_ID_150_CRC 254



#define MAVLINK_MESSAGE_INFO_CB_APP_CONTROL { \
	"CB_APP_CONTROL", \
	3, \
	{  { "app_id", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_cb_app_control_t, app_id) }, \
         { "action", NULL, MAVLINK_TYPE_UINT8_T, 0, 1, offsetof(mavlink_cb_app_control_t, action) }, \
         { "mavlink_version", NULL, MAVLINK_TYPE_UINT8_T, 0, 2, offsetof(mavlink_cb_app_control_t, mavlink_version) }, \
         } \
}


/**
 * @brief Pack a cb_app_control message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param app_id ID of the application; not static.
 * @param action Action to carry out. Defined in CB_APP_CONTROL_ACTION.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_cb_app_control_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
						       uint8_t app_id, uint8_t action)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_CB_APP_CONTROL_LEN];
	_mav_put_uint8_t(buf, 0, app_id);
	_mav_put_uint8_t(buf, 1, action);
	_mav_put_uint8_t(buf, 2, 2);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CB_APP_CONTROL_LEN);
#else
	mavlink_cb_app_control_t packet;
	packet.app_id = app_id;
	packet.action = action;
	packet.mavlink_version = 2;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CB_APP_CONTROL_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_CB_APP_CONTROL;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_CB_APP_CONTROL_LEN, MAVLINK_MSG_ID_CB_APP_CONTROL_CRC);
#else
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_CB_APP_CONTROL_LEN);
#endif
}

/**
 * @brief Pack a cb_app_control message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param app_id ID of the application; not static.
 * @param action Action to carry out. Defined in CB_APP_CONTROL_ACTION.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_cb_app_control_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
							   mavlink_message_t* msg,
						           uint8_t app_id,uint8_t action)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_CB_APP_CONTROL_LEN];
	_mav_put_uint8_t(buf, 0, app_id);
	_mav_put_uint8_t(buf, 1, action);
	_mav_put_uint8_t(buf, 2, 2);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CB_APP_CONTROL_LEN);
#else
	mavlink_cb_app_control_t packet;
	packet.app_id = app_id;
	packet.action = action;
	packet.mavlink_version = 2;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CB_APP_CONTROL_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_CB_APP_CONTROL;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_CB_APP_CONTROL_LEN, MAVLINK_MSG_ID_CB_APP_CONTROL_CRC);
#else
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_CB_APP_CONTROL_LEN);
#endif
}

/**
 * @brief Encode a cb_app_control struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param cb_app_control C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_cb_app_control_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_cb_app_control_t* cb_app_control)
{
	return mavlink_msg_cb_app_control_pack(system_id, component_id, msg, cb_app_control->app_id, cb_app_control->action);
}

/**
 * @brief Encode a cb_app_control struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param cb_app_control C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_cb_app_control_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_cb_app_control_t* cb_app_control)
{
	return mavlink_msg_cb_app_control_pack_chan(system_id, component_id, chan, msg, cb_app_control->app_id, cb_app_control->action);
}

/**
 * @brief Send a cb_app_control message
 * @param chan MAVLink channel to send the message
 *
 * @param app_id ID of the application; not static.
 * @param action Action to carry out. Defined in CB_APP_CONTROL_ACTION.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_cb_app_control_send(mavlink_channel_t chan, uint8_t app_id, uint8_t action)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_CB_APP_CONTROL_LEN];
	_mav_put_uint8_t(buf, 0, app_id);
	_mav_put_uint8_t(buf, 1, action);
	_mav_put_uint8_t(buf, 2, 2);

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CB_APP_CONTROL, buf, MAVLINK_MSG_ID_CB_APP_CONTROL_LEN, MAVLINK_MSG_ID_CB_APP_CONTROL_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CB_APP_CONTROL, buf, MAVLINK_MSG_ID_CB_APP_CONTROL_LEN);
#endif
#else
	mavlink_cb_app_control_t packet;
	packet.app_id = app_id;
	packet.action = action;
	packet.mavlink_version = 2;

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CB_APP_CONTROL, (const char *)&packet, MAVLINK_MSG_ID_CB_APP_CONTROL_LEN, MAVLINK_MSG_ID_CB_APP_CONTROL_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CB_APP_CONTROL, (const char *)&packet, MAVLINK_MSG_ID_CB_APP_CONTROL_LEN);
#endif
#endif
}

#endif

// MESSAGE CB_APP_CONTROL UNPACKING


/**
 * @brief Get field app_id from cb_app_control message
 *
 * @return ID of the application; not static.
 */
static inline uint8_t mavlink_msg_cb_app_control_get_app_id(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint8_t(msg,  0);
}

/**
 * @brief Get field action from cb_app_control message
 *
 * @return Action to carry out. Defined in CB_APP_CONTROL_ACTION.
 */
static inline uint8_t mavlink_msg_cb_app_control_get_action(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint8_t(msg,  1);
}

/**
 * @brief Get field mavlink_version from cb_app_control message
 *
 * @return MAVLink version
 */
static inline uint8_t mavlink_msg_cb_app_control_get_mavlink_version(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint8_t(msg,  2);
}

/**
 * @brief Decode a cb_app_control message into a struct
 *
 * @param msg The message to decode
 * @param cb_app_control C-struct to decode the message contents into
 */
static inline void mavlink_msg_cb_app_control_decode(const mavlink_message_t* msg, mavlink_cb_app_control_t* cb_app_control)
{
#if MAVLINK_NEED_BYTE_SWAP
	cb_app_control->app_id = mavlink_msg_cb_app_control_get_app_id(msg);
	cb_app_control->action = mavlink_msg_cb_app_control_get_action(msg);
	cb_app_control->mavlink_version = mavlink_msg_cb_app_control_get_mavlink_version(msg);
#else
	memcpy(cb_app_control, _MAV_PAYLOAD(msg), MAVLINK_MSG_ID_CB_APP_CONTROL_LEN);
#endif
}
