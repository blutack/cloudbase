// MESSAGE CB_APP_LIST_REQUEST PACKING

#define MAVLINK_MSG_ID_CB_APP_LIST_REQUEST 151

typedef struct __mavlink_cb_app_list_request_t
{
 uint8_t app_id; ///< PADDING
 uint8_t mavlink_version; ///< MAVLink version
} mavlink_cb_app_list_request_t;

#define MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_LEN 2
#define MAVLINK_MSG_ID_151_LEN 2

#define MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_CRC 43
#define MAVLINK_MSG_ID_151_CRC 43



#define MAVLINK_MESSAGE_INFO_CB_APP_LIST_REQUEST { \
	"CB_APP_LIST_REQUEST", \
	2, \
	{  { "app_id", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_cb_app_list_request_t, app_id) }, \
         { "mavlink_version", NULL, MAVLINK_TYPE_UINT8_T, 0, 1, offsetof(mavlink_cb_app_list_request_t, mavlink_version) }, \
         } \
}


/**
 * @brief Pack a cb_app_list_request message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param app_id PADDING
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_cb_app_list_request_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
						       uint8_t app_id)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_LEN];
	_mav_put_uint8_t(buf, 0, app_id);
	_mav_put_uint8_t(buf, 1, 2);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_LEN);
#else
	mavlink_cb_app_list_request_t packet;
	packet.app_id = app_id;
	packet.mavlink_version = 2;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_CB_APP_LIST_REQUEST;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_LEN, MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_CRC);
#else
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_LEN);
#endif
}

/**
 * @brief Pack a cb_app_list_request message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param app_id PADDING
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_cb_app_list_request_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
							   mavlink_message_t* msg,
						           uint8_t app_id)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_LEN];
	_mav_put_uint8_t(buf, 0, app_id);
	_mav_put_uint8_t(buf, 1, 2);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_LEN);
#else
	mavlink_cb_app_list_request_t packet;
	packet.app_id = app_id;
	packet.mavlink_version = 2;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_CB_APP_LIST_REQUEST;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_LEN, MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_CRC);
#else
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_LEN);
#endif
}

/**
 * @brief Encode a cb_app_list_request struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param cb_app_list_request C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_cb_app_list_request_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_cb_app_list_request_t* cb_app_list_request)
{
	return mavlink_msg_cb_app_list_request_pack(system_id, component_id, msg, cb_app_list_request->app_id);
}

/**
 * @brief Encode a cb_app_list_request struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param cb_app_list_request C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_cb_app_list_request_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_cb_app_list_request_t* cb_app_list_request)
{
	return mavlink_msg_cb_app_list_request_pack_chan(system_id, component_id, chan, msg, cb_app_list_request->app_id);
}

/**
 * @brief Send a cb_app_list_request message
 * @param chan MAVLink channel to send the message
 *
 * @param app_id PADDING
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_cb_app_list_request_send(mavlink_channel_t chan, uint8_t app_id)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_LEN];
	_mav_put_uint8_t(buf, 0, app_id);
	_mav_put_uint8_t(buf, 1, 2);

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CB_APP_LIST_REQUEST, buf, MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_LEN, MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CB_APP_LIST_REQUEST, buf, MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_LEN);
#endif
#else
	mavlink_cb_app_list_request_t packet;
	packet.app_id = app_id;
	packet.mavlink_version = 2;

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CB_APP_LIST_REQUEST, (const char *)&packet, MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_LEN, MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CB_APP_LIST_REQUEST, (const char *)&packet, MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_LEN);
#endif
#endif
}

#endif

// MESSAGE CB_APP_LIST_REQUEST UNPACKING


/**
 * @brief Get field app_id from cb_app_list_request message
 *
 * @return PADDING
 */
static inline uint8_t mavlink_msg_cb_app_list_request_get_app_id(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint8_t(msg,  0);
}

/**
 * @brief Get field mavlink_version from cb_app_list_request message
 *
 * @return MAVLink version
 */
static inline uint8_t mavlink_msg_cb_app_list_request_get_mavlink_version(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint8_t(msg,  1);
}

/**
 * @brief Decode a cb_app_list_request message into a struct
 *
 * @param msg The message to decode
 * @param cb_app_list_request C-struct to decode the message contents into
 */
static inline void mavlink_msg_cb_app_list_request_decode(const mavlink_message_t* msg, mavlink_cb_app_list_request_t* cb_app_list_request)
{
#if MAVLINK_NEED_BYTE_SWAP
	cb_app_list_request->app_id = mavlink_msg_cb_app_list_request_get_app_id(msg);
	cb_app_list_request->mavlink_version = mavlink_msg_cb_app_list_request_get_mavlink_version(msg);
#else
	memcpy(cb_app_list_request, _MAV_PAYLOAD(msg), MAVLINK_MSG_ID_CB_APP_LIST_REQUEST_LEN);
#endif
}
