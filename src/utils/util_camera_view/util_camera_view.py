#!/usr/bin/env python

import cv
import nanomsg as nm

running = True
s = nm.Socket(nm.SUB)

def setup():
    s.connect("ipc:///tmp/pubsub.ipc")
    s.set_string_option(nm.SUB, nm.SUB_SUBSCRIBE, '')

    cv.NamedWindow('Util Viewer', cv.CV_WINDOW_NORMAL)

def serve():
    while(running):
        buf = s.recv()
        img = cv.CreateImage((640, 480), cv.IPL_DEPTH_8U, 3)
        cv.SetData(img, buf, 640 * 3)
        cv.ShowImage('Util Viewer', img)
        cv.WaitKey(10)

def teardown():
    cv.destroyAllWindows()

setup()
serve()
teardown()
