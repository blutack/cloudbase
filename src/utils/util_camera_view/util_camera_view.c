#include <signal.h>
#include <stdbool.h>

#include <cv.h>
#include <highgui.h>

#include <nanomsg/nn.h>
#include <nanomsg/pubsub.h>

#define WINDOW_NAME "Cloudbase Image Viewer"

volatile bool serving = true;

int sock;

CvSize size;
IplImage* img;

void int_handler(int dummy) 
{
    serving = false;
}

int setup (const char *endpoint_url, const int video_device)
{
    signal(SIGINT, int_handler);

    cvNamedWindow(WINDOW_NAME, 0); // create window

    sock = nn_socket (AF_SP, NN_SUB);
    assert (nn_setsockopt (sock, NN_SUB, NN_SUB_SUBSCRIBE, "", 0) >= 0);
	assert (nn_connect (sock, endpoint_url) >= 0);
}

int serve ()
{
    int c = 0;
    int bytes = 0;

    size.height = 480;
    size.width = 640;

    while (serving)
    {
        char *buf = NULL;
        int bytes = nn_recv (sock, &buf, NN_MSG, 0);

        if(bytes >= 0) 
        {
            img = cvCreateImageHeader(size, IPL_DEPTH_8U, 3);
            cvSetData(img, buf, 3*640);
            cvShowImage(WINDOW_NAME, img);
            bytes = nn_send (sock, img->imageData, img->imageSize, 0);
        }

        nn_freemsg(buf);

        cvWaitKey(10);
    }
    return nn_shutdown (sock, 0);
}

int teardown()
{
    cvDestroyWindow(WINDOW_NAME);
}

int main (const int argc, const char **argv)
{
    //assert(argc == 2);

    setup("ipc:///tmp/pubsub.ipc", 0);
    serve();
    teardown();

    return 0;
}
