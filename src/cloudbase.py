#!/usr/bin/env python
import sys
sys.path.append("./lib")

import subprocess, os, atexit, time
import ConfigParser as configparser
import cloudbase_client, mavlink

SUPERVISOR_CONFIG_FILE = 'cloudbase.conf'

@atexit.register
def kill_subprocesses():
    print 'Shutting down supervisord...'
    time.sleep(1)
    subprocess.check_call(['supervisorctl', '-c', SUPERVISOR_CONFIG_FILE, 'shutdown'])
    os.remove(SUPERVISOR_CONFIG_FILE)

class CBApp:
    name = None
    path = None
    app_id = None
    environment = None
    streams = None

    def __init__(self, appdir, app_id):
        self.path = appdir
        self.name = os.path.basename(appdir)
        self.app_id = app_id
        self.streams = []
        self.environment = [
            'CB_ENDPOINT=ipc:///tmp/apps.ipc',
            'PYTHONUNBUFFERED=YES',
            'PYTHONPATH=' + os.getcwd() + '/lib'
        ]
        
    def add_to_config(self, config):
        parser = configparser.SafeConfigParser()
        parser.read(self.path + '/app.ini')

        section_name = 'program:' + self.path
        config.add_section(section_name)

        config.set(section_name, 'directory', self.path)
        config.set(section_name, 'command', self.path + '/' + parser.get('cloudbase:app', 'command'))
        
        parser.remove_option('cloudbase:app', 'command')

        if parser.has_option('cloudbase:app', 'streams'):
            for i in range(0, int(parser.get('cloudbase:app', 'streams'))):
                self.streams.append('ipc:///tmp/' + self.name + str(i + 1) + '.ipc')
        
        for idx, stream in enumerate(self.streams):
            self.environment.append('CB_STREAM' + str(idx + 1) + "=" + stream)

        config.set(section_name, 'environment', ','.join(self.environment))

        print self.name
        print self.environment

        for i in parser.items('cloudbase:app'):
            config.set(section_name, i[0], i[1])
    
class Cloudbase:
    client = None
    apps = []

    def __init__(self):
        self.client = cloudbase_client.CBClient(initial = True)
        self.generate_config()

    def generate_config(self):
        config = configparser.ConfigParser()
        config.add_section('THIS FILE IS AUTOMATICALLY REGENERATED')
        config.add_section('supervisord')
        config.add_section('unix_http_server')
        config.set('unix_http_server', 'file', '/tmp/supervisor.sock')
        config.add_section('inet_http_server')
        config.set('inet_http_server', 'port', '127.0.0.1:9001')
        config.add_section('rpcinterface:supervisor')
        config.set('rpcinterface:supervisor', 'supervisor.rpcinterface_factory', 
            'supervisor.rpcinterface:make_main_rpcinterface')
        config.add_section('supervisorctl')
        config.set('supervisorctl', 'serverurl', 'unix:///tmp/supervisor.sock')

        for idx, appdir in enumerate(os.listdir('apps')):
            path =  os.path.join(os.getcwd(), 'apps', appdir)

            if os.path.isdir(path):
                app = CBApp(path, idx)
                app.add_to_config(config)                
                self.apps.append(app) 

        with open(SUPERVISOR_CONFIG_FILE, 'w') as configfile:
            config.write(configfile)

    def handle_app_list(self, msg):
        for app in self.apps:
            msg = self.__list_response(app)
            self.client.send_msg(msg)
        
    def __list_response(self, app):
        return mavlink.MAVLink_cb_app_list_response_message(
            app.app_id,
            app.name,
            1,
            1,
            app.streams[0] if len(app.streams) > 0 else "",
            app.streams[1] if len(app.streams) > 1 else "",
            app.streams[2] if len(app.streams) > 2 else "",
            app.streams[3] if len(app.streams) > 3 else "",
            mavlink_version=2
        )

    def serve(self, args=None, test=False):
        print 'Booting supervisord...'
        subprocess.Popen(['supervisord', '-c', SUPERVISOR_CONFIG_FILE])
        self.client.notify_msg(mavlink.MAVLINK_MSG_ID_CB_APP_LIST_REQUEST, self.handle_app_list)
        self.client.loop()

cb = Cloudbase()
cb.serve()
