#include <signal.h>
#include <stdbool.h>

#include <cv.h>
#include <highgui.h>

#include <nanomsg/nn.h>
#include <nanomsg/pubsub.h>

#define WINDOW_NAME "Cloudbase Capture"
#define GUI 1

volatile bool serving = true;

int sock;

IplImage* img;
CvCapture* cv_cap;

void int_handler (int dummy) 
{
    serving = false;
}

int setup (const char *endpoint_url, const int video_device)
{
    signal (SIGINT, int_handler);

    cv_cap = cvCaptureFromCAM(0);

    #ifdef GUI
        cvNamedWindow(WINDOW_NAME, 0);
    #endif

    sock = nn_socket (AF_SP, NN_PUB);
    assert (sock >= 0);
    assert (nn_bind (sock, endpoint_url) >= 0);
}

int serve ()
{
    int c = 0;
    int bytes = 0;

    while (serving)
    {
        img = cvQueryFrame (cv_cap);

        if (img != 0) 
        {
            #ifdef GUI
                cvShowImage (WINDOW_NAME, img);
            #endif
            bytes = nn_send (sock, img->imageData, img->imageSize, 0);
            assert (bytes == img->imageSize);
        }

        cvWaitKey (10);
    }

    return nn_shutdown (sock, 0);
}

int teardown ()
{
    cvReleaseCapture (&cv_cap);
    #ifdef GUI        
        cvDestroyWindow (WINDOW_NAME);
    #endif
}

int main (const int argc, const char **argv)
{
    //assert(argc == 2);

    setup ("ipc:///tmp/pubsub.ipc", 0);
    serve ();
    teardown ();

    return 0;
}
